package com.jsite.modules.test.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jsite.common.persistence.TreeEntity;

/**
 * 树结构表生成Entity
 * @author liuruijun
 * @version 2019-01-08
 */
public class TestTree extends TreeEntity<TestTree> {
	
	private static final long serialVersionUID = 1L;

	
	public TestTree() {
		super();
	}

	public TestTree(String id){
		super(id);
	}

	@JsonIgnore
	@Override
	public boolean getIsRoot() {
		return ROOT_ID.equals(parent.getId());
	}

}